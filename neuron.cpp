#include "neuron.h"

#include <algorithm>
#include <random>

Neuron::Neuron(size_t inputs_n)
  : weights(inputs_n + 1)
  , net(0) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(-1, 1);
    std::generate(weights.begin(), weights.end(), [&]() -> double { return randDistrib(gen); });
}

double Neuron::evaluate(const std::vector<double> &inputs) {
    if (inputs.size() + 1 != weights.size()) {
        throw std::length_error("Inputs size must be equl to weights size.");
    }
    net = 0.0;
    for (size_t i = 0; i < inputs.size(); ++i) {
        net += weights[i] * inputs[i];
    }
    net -= weights[inputs.size()];
    out = sigmoid(net);
    return out;
}

double Neuron::evaluate_d() {
    return sigmoid_d(net);
}
