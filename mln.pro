TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++0x -pthread
LIBS += -pthread

SOURCES += \
        main.cpp \
    neuron.cpp \
    mln.cpp \
    mnist_reader.cpp

HEADERS += \
    neuron.h \
    mln.h \
    mnist_reader.h

DISTFILES += \
    .clang-format \
    .gitignore
