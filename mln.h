#ifndef ANN_H
#define ANN_H

#include "neuron.h"

#include <vector>

#define Test std::pair<std::vector<double>, std::vector<double>>
#define Result std::pair<double, size_t>

class MLN {
private:
    size_t useless;
    std::vector<size_t> architecture;
    void reinit(const std::vector<size_t> &architecture);

public:
    std::vector<std::vector<Neuron>> layers;
    double error;

    MLN() {}
    MLN(const std::vector<size_t> &architecture);

    std::vector<double> evaluate(std::vector<double> inputs);
    Result train(const std::vector<Test> &tests, const double learning_rate, const double max_error, const size_t max_epochs);
    double fit(const std::vector<Test> &tests, const double learning_rate);
    void calculate_sensitivities(const Test &test);
    void update_weights(const Test &test, const double learning_rate);
    double acc(const std::vector<Test> &tests);
};

#endif // ANN_H
