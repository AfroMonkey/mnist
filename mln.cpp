#include "mln.h"

#include <iostream>

using namespace std;

MLN::MLN(const std::vector<size_t> &architecture)
  : useless(0)
  , architecture(architecture) {
    if (architecture.size() < 2) {
        throw std::length_error("Architecture must have 2 layers at least.");
    }
    reinit(architecture);
}

void MLN::reinit(const std::vector<size_t> &architecture) {
    layers = std::vector<std::vector<Neuron>>(architecture.size() - 1);
    for (size_t m = 1; m < architecture.size(); ++m) {
        for (size_t _ = 0; _ < architecture[m]; ++_) {
            layers[m - 1].push_back(Neuron(architecture[m - 1]));
        }
    }
}

std::vector<double> MLN::evaluate(std::vector<double> inputs) {
    std::vector<double> outputs;
    for (auto &layer : layers) {
        outputs.clear();
        outputs.resize(layer.size());
        for (size_t i = 0; i < layer.size(); ++i) {
            auto &neuron = layer[i];
            outputs[i] = neuron.evaluate(inputs);
        }
        inputs = outputs;
    }
    return outputs;
}

Result MLN::train(const std::vector<Test> &tests, const double learning_rate, const double max_error, const size_t max_epochs) {
    size_t epochs = 0;
    error = max_error;
    while (error >= max_error and ++epochs < max_epochs) {
        fit(tests, learning_rate);
    }
    return {error, epochs};
}

double MLN::fit(const std::vector<Test> &tests, const double learning_rate) {
    double prev = error;
    error = 0.0;
    for (const auto &test : tests) {
        evaluate(test.first);
        calculate_sensitivities(test);
        update_weights(test, learning_rate);
    }
    if (abs(prev - error) < 0.000001) {
        ++useless;
    } else {
        useless = 0;
    }
    if (useless > 1000) {
        reinit(architecture);
    }
    return error;
}

void MLN::calculate_sensitivities(const Test &test) {
    // s^M
    for (size_t i = 0; i < layers.back().size(); ++i) {
        auto &neuron = layers.back()[i];
        double test_error = test.second[i] - neuron.out;
        error += (test_error * test_error);
        neuron.sensitivity = -2 * neuron.evaluate_d() * test_error;
    }
    error /= 2;
    // s^m
    for (long m_long = static_cast<long>(layers.size()) - 2; m_long >= 0; --m_long) {
        size_t m = static_cast<size_t>(m_long); // Just for beautify code
        for (size_t i = 0; i < layers[m].size(); ++i) {
            layers[m][i].sensitivity = 0;
            for (size_t k = 0; k < layers[m + 1].size(); ++k) {
                layers[m][i].sensitivity += layers[m][i].evaluate_d() * layers[m + 1][k].weights[i + 1] * layers[m + 1][k].sensitivity;
            }
        }
    }
}

void MLN::update_weights(const Test &test, const double learning_rate) {
    auto inputs = test.first;
    for (auto &layer : layers) {
        std::vector<double> outputs(layer.size());
        for (size_t i = 0; i < layer.size(); ++i) {
            auto &neuron = layer[i];
            outputs[i] = neuron.out;
            auto d = -learning_rate * neuron.sensitivity;
            for (size_t w = 0; w < inputs.size(); ++w) {
                neuron.weights[w] += d * inputs[w];
            }
            neuron.weights.back() -= d;
        }
        inputs = outputs;
    }
}

double MLN::acc(const std::vector<Test> &tests) {
    double a = 0;
    for (auto &test : tests) {
        auto res = evaluate(test.first);
        size_t d;
        size_t max;
        for (d = max = 0; d < res.size(); ++d) {
            if (res[d] > res[max]) {
                max = d;
            }
        }
        if (test.second[max] < 1) {
            --a;
        }
    }
    a/= tests.size();
    return a + 1;
}
