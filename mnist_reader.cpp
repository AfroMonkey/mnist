#include "mnist_reader.h"

#include <fstream>
#include <iostream>

using namespace std;

#define LABELS_FILE_MAGIC_NUMBER_OFFSET 4
#define LABELS_FILE_NUMBER_OF_ITEMS_OFFSET 4
#define IMAGES_FILE_MAGIC_NUMBER_OFFSET 4
#define IMAGES_FILE_NUMBER_OF_ITEMS_OFFSET 4
#define IMAGES_FILE_NUMBER_OF_ROWS_OFFSET 4
#define IMAGES_FILE_NUMBER_OF_COLS_OFFSET 4
#define IMAGES_FILE_NUMBER_OF_ROWS 28
#define IMAGES_FILE_NUMBER_OF_COLS 28

std::vector<Test> mnist_read(const std::string images_path, const std::string labels_path, const size_t sample_frequency) {
    std::vector<Test> tests;
    std::fstream images_file(images_path);
    std::fstream labels_file(labels_path);
    if (images_file and labels_file) {
        labels_file.seekg(LABELS_FILE_MAGIC_NUMBER_OFFSET + LABELS_FILE_NUMBER_OF_ITEMS_OFFSET);

        images_file.seekg(IMAGES_FILE_MAGIC_NUMBER_OFFSET + IMAGES_FILE_NUMBER_OF_ITEMS_OFFSET + IMAGES_FILE_NUMBER_OF_ROWS_OFFSET + IMAGES_FILE_NUMBER_OF_COLS_OFFSET);

        uint8_t c;
        unsigned char image[IMAGES_FILE_NUMBER_OF_ROWS * IMAGES_FILE_NUMBER_OF_COLS];
        size_t s = 0;
        while (true) {
            labels_file.read(reinterpret_cast<char *>(&c), sizeof(c));
            if (labels_file.eof()) {
                break;
            }
            images_file.read(reinterpret_cast<char *>(&image), sizeof(image));
            std::vector<double> inputs(image, image + IMAGES_FILE_NUMBER_OF_ROWS * IMAGES_FILE_NUMBER_OF_COLS);
            double mean = 0;
            if (++s % sample_frequency) {
                continue;
            }
            for (auto &coord : inputs) {
                coord /= 255;
                mean += coord;
            }
            mean /= (IMAGES_FILE_NUMBER_OF_ROWS * IMAGES_FILE_NUMBER_OF_COLS);
            for (auto &coord : inputs) {
                coord -= mean;
            }
            std::vector<double> number(10, 0);
            number[c] = 1;
            tests.push_back({inputs, number});
        }
    }
    return tests;
}

void print(const Test &test) {
    auto &data = test.first;
    double mean = 0;
    for (auto &pixel : data) {
        mean += pixel;
    }
    mean /= data.size();
    for (size_t y = 0; y < IMAGES_FILE_NUMBER_OF_ROWS; ++y) {
        for (size_t x = 0; x < IMAGES_FILE_NUMBER_OF_COLS; ++x) {
            char pixel = data[y * IMAGES_FILE_NUMBER_OF_ROWS + x] > mean ? 'x' : ' ';
            cout << pixel;
        }
        cout << endl;
    }
}
