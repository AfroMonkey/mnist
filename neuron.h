#ifndef NEURON_H
#define NEURON_H

#include <cmath>
#include <functional>
#include <tuple>
#include <vector>

class Neuron {
private:
    static double sigmoid(double val) {
        return 1 / (1 + std::exp(-val));
    }

    static double sigmoid_d(double val) {
        double s = sigmoid(val);
        return s * (1 - s);
    }

public:
    std::vector<double> weights;
    double net;
    double out;
    double sensitivity;

    Neuron(size_t inputs_n);

    double evaluate(const std::vector<double> &inputs);
    double evaluate_d();
};

#endif // NEURON_H
