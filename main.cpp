#include "mln.h"
#include "mnist_reader.h"

#include <iostream>

#define HIDDEN_LAYERS 100
#define LEARNING_RATE 0.5
#define MAX_ERROR 0.0001
#define MAX_EPOCHS 100000
#define NUMBERS 10
#define SAMPLE_FREQUENCY 1

using namespace std;
static auto trainings = mnist_read("/home/moy/CUCEI/Seminario de Inteligencia Artificial II/P6 - MNIST/data/train_images", "/home/moy/CUCEI/Seminario de Inteligencia Artificial II/P6 - MNIST/data/train_labels", SAMPLE_FREQUENCY);
static auto tests = mnist_read("/home/moy/CUCEI/Seminario de Inteligencia Artificial II/P6 - MNIST/data/test_images", "/home/moy/CUCEI/Seminario de Inteligencia Artificial II/P6 - MNIST/data/test_labels");
static const std::vector<size_t> arch{trainings[0].first.size(), HIDDEN_LAYERS, trainings[0].second.size()};
static MLN mln(arch);

void train() {
    double error;
    for (size_t epoch = 0; epoch < MAX_EPOCHS; ++epoch) {
        error = mln.fit(trainings, LEARNING_RATE);
        cout << " E=" << epoch << " A=" << error << endl;
        if (error < MAX_ERROR) {
            break;
        }
    }
}

void test() {
    size_t t;
    do {
        cout << "Test: ";
        cin >> t;
        print(tests[t]);
        auto probabilities = mln.evaluate(tests[t].first);
        for (size_t n = 0; n < NUMBERS; ++n) {
            cout << "N=" << n << " P=" << probabilities[n] << endl;
        }
    } while (t);
}

int main() {
    train();
    cout << mln.acc(tests) << endl;
    test();
    return 0;
}
