#ifndef MNIST_READER_H
#define MNIST_READER_H

#include "mln.h"

std::vector<Test> mnist_read(const std::string images_path, const std::string labels_path, const size_t sample_frequency = 1);
void print(const Test &test);

#endif // MNIST_READER_H
